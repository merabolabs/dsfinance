import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateReportsComponent } from './state-reports.component';

describe('StateReportsComponent', () => {
  let component: StateReportsComponent;
  let fixture: ComponentFixture<StateReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
