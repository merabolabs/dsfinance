import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common'
import { MatSnackBar, MatPaginator } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from "../../services";
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-city-reports',
  templateUrl: './city-reports.component.html',
  styleUrls: ['./city-reports.component.css']
})
export class CityReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
