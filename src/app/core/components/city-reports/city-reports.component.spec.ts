import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityReportsComponent } from './city-reports.component';

describe('CityReportsComponent', () => {
  let component: CityReportsComponent;
  let fixture: ComponentFixture<CityReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
