import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WareHouseTransactionSummeryComponent } from './ware-house-transaction-summery.component';

describe('WareHouseTransactionSummeryComponent', () => {
  let component: WareHouseTransactionSummeryComponent;
  let fixture: ComponentFixture<WareHouseTransactionSummeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WareHouseTransactionSummeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WareHouseTransactionSummeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
