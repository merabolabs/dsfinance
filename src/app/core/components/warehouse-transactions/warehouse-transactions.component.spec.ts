import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseTransactionsComponent } from './warehouse-transactions.component';

describe('WarehouseTransactionsComponent', () => {
  let component: WarehouseTransactionsComponent;
  let fixture: ComponentFixture<WarehouseTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
