import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common'
import { MatSnackBar, MatPaginator  } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from "../../services";
import { tap } from 'rxjs/operators';


const TAG = "WarehouseTransactionsComponent : ";

@Component({
  selector: 'app-warehouse-transactions',
  templateUrl: './warehouse-transactions.component.html',
  styleUrls: ['./warehouse-transactions.component.css']
})
export class WarehouseTransactionsComponent implements AfterViewInit, OnInit {

  qryFilter:string="";
  qryPage:string="";
  filterForm: FormGroup;
  loading:boolean=false;
  transactions:any[]=[];
  wareHouses:any[]=[];

  displayedColumns: string[] = ['position', "warehouse", "warehouse_id", "transaction_value", "type", "sub_type", "created_date" ];

   //pagination
   totalPages:number=0;
   totalItems:number=0;
   currentPage:number=1;

  @ViewChild(MatPaginator,{ static: false }) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public datePipe: DatePipe,

  ) { 
    this.getWareHouses();

  }

  ngOnInit() {
    this.qryPage="&page=1";
    this.initializeForm();
    this.getTransactions();
  }

  initializeForm() {
    this.filterForm = this.formBuilder.group({
      warehouse:[''],
      warehouse_id: [''],
      type: [''],
      start_date:"",
      end_date:"",
    });
  }
  get f() { return this.filterForm.controls; }

  ngAfterViewInit(): void {
    this.paginator.page
            .pipe(
                tap(() => {
                  this.qryPage = `&page=${this.paginator.pageIndex+1}`;
                  this.getTransactions();
                })
            )
            .subscribe();
  }

  getWareHouses(){
    // this.loading = true;
    this.dataService.getWareHouses()
      .subscribe(res => {
        // this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        // this.openSnackBar(res.msg, "");
        if (res.status) {
          this.wareHouses = res.data;
        }

      });
  }

  getTransactions(){
    var qryParam = this.qryPage.concat(this.qryFilter); 
    this.loading = true;
    this.dataService.getTransactions({qryParam})
    .subscribe(res=>{
      this.loading =false;
      console.log(TAG+"Res : "+JSON.stringify(res));
      this.openSnackBar(res.msg,"");
      if(res.status){
       this.transactions = res.data;
       this.totalPages = res.total_pages;
       this.totalItems = res.total;
       this.currentPage = res.page;
      }

    });
  }

  onApplyFilter(){
    // console.log(TAG+"VArification :"+this.f.verified_status.value);
    
   let filter = { 
     warehouse:this.f.warehouse.value != "undefined" ? this.f.warehouse.value : "" ,
     type: typeof this.f.type.value != "undefined" ? this.f.type.value : "",
     start_date: this.formateDate(new Date(this.f.start_date.value)),
     end_date: this.formateDate(new Date(this.f.end_date.value)),
    };
    this.qryFilter = "";
    
    this.qryFilter = this.qryFilter.concat(filter.warehouse==""? "" :`&warehouse_id=${filter.warehouse}`);
    this.qryFilter = this.qryFilter.concat(filter.type==""? "" :`&type=${filter.type}`);
    this.qryFilter = this.qryFilter.concat(filter.start_date==""? "" :`&start_date=${filter.start_date}`);
    this.qryFilter = this.qryFilter.concat(filter.end_date==""? "" :`&end_date=${filter.end_date}`);
    
    this.getTransactions();
    console.log(TAG+"Applied Filter :"+JSON.stringify(filter));    
    console.log(TAG+"Applied Filter :"+JSON.stringify(this.qryFilter));
  }

  onClickAction(index){
    // this.router.navigate(['./warehouse_transaction_summery',{data:this.transactions[index]}],{relativeTo:this.route});
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  formateDate(date:Date){
    if(!isNaN(date.getMilliseconds()))
      return this.datePipe.transform(date, 'yyyy-MM-dd');
    else
      return "";  
  }

}
