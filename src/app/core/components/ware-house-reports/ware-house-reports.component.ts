import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common'
import { MatSnackBar, MatPaginator } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from "../../services";
import { tap } from 'rxjs/operators';

const TAG = "WareHouseReportsComponent : ";

@Component({
  selector: 'app-ware-house-reports',
  templateUrl: './ware-house-reports.component.html',
  styleUrls: ['./ware-house-reports.component.css']
})
export class WareHouseReportsComponent implements OnInit {

  qryFilter:string="";
  qryPage:string="";
  filterForm: FormGroup;
  loading:boolean=false;
  reports:any[]=[];

  displayedColumns: string[] = ['position', "warehouse", "totalQuantity", "gmv", "discount_amount", "state", "city",];


  constructor(
    private dataService: DataService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    public datePipe: DatePipe,
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.qryPage = "&page=1";
    this.getReports();
  }

  initializeForm() {
    this.filterForm = this.formBuilder.group({
      warehouse: [''],
      state: [''],
      city: [''],
    });
  }
  get f() { return this.filterForm.controls; }

  getReports() {
    // console.log(TAG+"GET GRN :+> PAGE:"+);

    // var qryPage = `&page=${page}`;  
    var qryParam = this.qryPage.concat(this.qryFilter);
    this.loading = true;
    this.dataService.getReportsData({ qryParam })
      .subscribe(res => {
        this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        this.openSnackBar(res.msg, "");
        if (res.status) {
          this.reports = res.data;
          // this.totalPages = res.total_pages;
          // this.totalItems = res.total;
          // this.currentPage = res.page;
        }

      });
  }


  onApplyFilter() {
    // console.log(TAG+"VArification :"+this.f.verified_status.value);

    let filter = {
      warehouse: this.f.warehouse.value.trim(),
      state: this.f.state.value.trim(),
      city: this.f.city.value.trim(),
   
    };
    this.qryFilter = "";

    this.qryFilter = this.qryFilter.concat(filter.warehouse == "" ? "" : `&warehouse=${filter.warehouse}`);
    this.qryFilter = this.qryFilter.concat(filter.state == "" ? "" : `&state=${filter.state}`);
    this.qryFilter = this.qryFilter.concat(filter.city == "" ? "" : `&city=${filter.city}`);

    this.getReports();
    console.log(TAG + "Applied Filter :" + JSON.stringify(filter));
    console.log(TAG + "Applied Filter :" + JSON.stringify(this.qryFilter));
  }

  //Util
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  formateDate(date: Date) {
    if (!isNaN(date.getMilliseconds()))
      return this.datePipe.transform(date, 'yyyy-MM-dd');
    else
      return "";
  }

}
