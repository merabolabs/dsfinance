import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WareHouseReportsComponent } from './ware-house-reports.component';

describe('WareHouseReportsComponent', () => {
  let component: WareHouseReportsComponent;
  let fixture: ComponentFixture<WareHouseReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WareHouseReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WareHouseReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
