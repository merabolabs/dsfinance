import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from "../../services";
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild('sidenav', { static: false }) sidenav: MatSidenav;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
  ) {

   }

  ngOnInit() {
    this.router.events.subscribe(event => {
      // close sidenav on routing
      this.sidenav.close();
    });
    
  }

  logout() {
    console.log("Logout.................");
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
