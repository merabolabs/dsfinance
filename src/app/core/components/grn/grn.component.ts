import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common'
import { MatSnackBar, MatPaginator, MatDialog, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService, DataService } from "../../services";
import { GrnVerificationDialogComponent } from "../grn-verification-dialog/grn-verification-dialog.component";
import { GrnDetailDialogComponent } from "../grn-detail-dialog/grn-detail-dialog.component";
import { tap } from 'rxjs/operators';

const TAG = "GRN: ";
@Component({
  selector: 'app-grn',
  templateUrl: './grn.component.html',
  styleUrls: ['./grn.component.css']
})
export class GrnComponent implements AfterViewInit, OnInit {
  
  
  grnVerificationDialogRef: MatDialogRef<GrnVerificationDialogComponent>;  
  grnDetailDialogRef: MatDialogRef<GrnDetailDialogComponent>;
  
  qryFilter:string="";
  qryPage:string="";
  filterForm: FormGroup;
  loading:boolean=false;
  grn:any[]=[];
  wareHouses:any[]=[];

  displayedColumns: string[] = ['position', "name", "warehouse", "po_id", "grn_id", "sku_count", "grnAmount", "grn_created_date", "verified_status", "action", "view_detail", "download_invoice"];
  
  @ViewChild(MatPaginator,{ static: false }) paginator: MatPaginator;

  //pagination
  totalPages:number=0;
  totalItems:number=0;
  currentPage:number=1;

  constructor(
    private authenticationService: AuthenticationService,
    private dataService: DataService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    public datePipe: DatePipe,
  ) { 
    this.getWareHouses();

  }

  ngOnInit() {
    this.initializeForm();
    console.log(TAG+" token :"+this.authenticationService.getUserToken);
    this.qryPage="&page=1";
    this.getGrn();
  }

  ngAfterViewInit(): void {
    this.paginator.page
            .pipe(
                tap(() => {
                  this.qryPage = `&page=${this.paginator.pageIndex+1}`;
                  this.getGrn();
                })
            )
            .subscribe();
            
  }

  initializeForm() {
    this.filterForm = this.formBuilder.group({
      vendor_name:[''],
      warehouse:[''],
      grn_id: [''],
      po_id: [''],
      verified_status: [''],
      start_date:"",
      end_date:"",
    });
  }
  get f() { return this.filterForm.controls; }


  openGRNVerificationDialog(index) {
    this.grnVerificationDialogRef = this.dialog.open(GrnVerificationDialogComponent,{data:this.grn[index]});

    this.grnVerificationDialogRef
        .afterClosed()
        .subscribe(action => {
          console.log(TAG+"Dialog Close :"+JSON.stringify(action));
          if(action!=null && action!="")
          this.verifyUnVerifyGRN(action);
        });
  }

  openGRNDetailDialog(index){
    this.grnDetailDialogRef = this.dialog.open(GrnDetailDialogComponent,{data:this.grn[index],width:"80%",});
  }

  getWareHouses(){
    // this.loading = true;
    this.dataService.getWareHouses()
      .subscribe(res => {
        // this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        // this.openSnackBar(res.msg, "");
        if (res.status) {
          this.wareHouses = res.data;
        }

      });
  }

  getGrn(){
    // console.log(TAG+"GET GRN :+> PAGE:"+);
    
    // var qryPage = `&page=${page}`;  
    var qryParam = this.qryPage.concat(this.qryFilter); 
    this.loading = true;
    this.dataService.getGrnData({qryParam})
    .subscribe(res=>{
      this.loading =false;
      console.log(TAG+"Res : "+JSON.stringify(res));
      this.openSnackBar(res.msg,"");
      if(res.status){
       this.grn = res.data;
       this.totalPages = res.total_pages;
       this.totalItems = res.total;
       this.currentPage = res.page;
      }

    });
  }

  //
  onApplyFilter(){
    // console.log(TAG+"VArification :"+this.f.verified_status.value);
    
   let filter = { 
     vendorName:this.f.vendor_name.value.trim(),
     warehouse:this.f.warehouse.value != "undefined" ? this.f.warehouse.value : "",
     grn_id:this.f.grn_id.value.trim(),
     po_id:this.f.po_id.value.trim(),
     verified_status: typeof this.f.verified_status.value != "undefined" ? this.f.verified_status.value : "",
     start_date: this.formateDate(new Date(this.f.start_date.value)),
     end_date: this.formateDate(new Date(this.f.end_date.value)),
    };
    this.qryFilter = "";
    
    this.qryFilter = this.qryFilter.concat(filter.vendorName==""? "" :`&name=${filter.vendorName}`);
    this.qryFilter = this.qryFilter.concat(filter.warehouse==""? "" :`&warehouse_id=${filter.warehouse}`);
    this.qryFilter = this.qryFilter.concat(filter.grn_id==""? "" :`&grn_id=${filter.grn_id}`);
    this.qryFilter = this.qryFilter.concat(filter.po_id==""? "" :`&po_id=${filter.po_id}`);
    this.qryFilter = this.qryFilter.concat(filter.verified_status=="" ? "" :`&verified_status=${filter.verified_status}`);
    this.qryFilter = this.qryFilter.concat(filter.start_date==""? "" :`&start_date=${filter.start_date}`);
    this.qryFilter = this.qryFilter.concat(filter.end_date==""? "" :`&end_date=${filter.end_date}`);
    
    this.getGrn();
    console.log(TAG+"Applied Filter :"+JSON.stringify(filter));
    console.log(TAG+"Applied Filter :"+JSON.stringify(this.qryFilter));
  }

  //Verify/Un-verify grn
  onClickAction(index:any){
    console.log(TAG+"GRN CLICK :"+index);
    this.openGRNVerificationDialog(index);
  }

  //Download Invoice
  downloadInvoice(index){
    window.open(this.grn[index].invoice_name);
  }

  //View Detail
  viewDetail(index){
    console.log(TAG+"View detail index  :"+index);
    this.openGRNDetailDialog(index);
  }




  //Verify/Un-verify grn
  verifyUnVerifyGRN(grn:any){
    const payload  = {
                      grn_id:grn.grn_id,
                      verified_status: grn.verified_status == "Verified" ? "0" : "1",
                    };
    
    console.log(TAG+"Verification status :"+grn.verified_status);
    console.log(TAG+"GRN Verification :"+JSON.stringify(payload));
                    
    this.loading = true;                
    this.dataService.verifyUnVerifyGRN({payload}).subscribe((res)=>{
      this.loading =false;
      this.openSnackBar(res.msg,"");
      if(res.status)
        this.getGrn();

    });                
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  formateDate(date:Date){
    if(!isNaN(date.getMilliseconds()))
      return this.datePipe.transform(date, 'yyyy-MM-dd');
    else
      return "";  
  }
}
