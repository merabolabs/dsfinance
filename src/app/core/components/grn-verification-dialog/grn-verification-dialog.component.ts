import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


const TAG = "GRN-VerificationDialog :";

@Component({
  selector: 'app-grn-verification-dialog',
  templateUrl: './grn-verification-dialog.component.html',
  styleUrls: ['./grn-verification-dialog.component.css']
})
export class GrnVerificationDialogComponent implements OnInit {
  
  
  constructor(
    private dialogRef: MatDialogRef<GrnVerificationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 

  }

  ngOnInit() {
    console.log(TAG+"data :"+JSON.stringify(this.data));
    
  }


  action(){
    this.dialogRef.close(this.data);
  }

}
