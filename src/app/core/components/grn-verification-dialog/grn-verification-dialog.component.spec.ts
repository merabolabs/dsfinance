import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrnVerificationDialogComponent } from './grn-verification-dialog.component';

describe('GrnVerificationDialogComponent', () => {
  let component: GrnVerificationDialogComponent;
  let fixture: ComponentFixture<GrnVerificationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrnVerificationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrnVerificationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
