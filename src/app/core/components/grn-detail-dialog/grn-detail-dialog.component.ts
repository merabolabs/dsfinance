import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

const TAG = "GrnDetailDialogComponent :";

@Component({
  selector: 'app-grn-detail-dialog',
  templateUrl: './grn-detail-dialog.component.html',
  styleUrls: ['./grn-detail-dialog.component.css']
})
export class GrnDetailDialogComponent implements OnInit {

  displayedColumns: string[] = ['position', "sku", "product_name", "buying_price", "actual_mrp", "invoice_qty", "discrepancy_qty",];
  
  skus:any[]=[];

  constructor(
    private dialogRef: MatDialogRef<GrnDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
   }

  ngOnInit() {
    console.log(TAG+"data :"+JSON.stringify(this.data));
    this.skus = this.data.skuData;
  }

}
