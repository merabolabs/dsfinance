import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrnDetailDialogComponent } from './grn-detail-dialog.component';

describe('GrnDetailDialogComponent', () => {
  let component: GrnDetailDialogComponent;
  let fixture: ComponentFixture<GrnDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrnDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrnDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
