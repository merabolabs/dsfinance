import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common'
import { MatSnackBar, MatPaginator } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from "../../services";
import { tap } from 'rxjs/operators';

const TAG = "OrderStatusReportsComponent : ";

@Component({
  selector: 'app-reports',
  templateUrl: './order.status.reports.component.html',
  styleUrls: ['./order.status.reports.component.css']
})
export class OrderStatusReportsComponent implements AfterViewInit, OnInit {

  qryFilter:string="";
  qryPage:string="";
  filterForm: FormGroup;
  loading:boolean=false;
  reports:any[]=[];

  //
  wareHouses:any[]=[];
  cities:any[]=[];
  states:any[]=[];

  displayedColumns: string[] = ['position', "warehouse", "totalQuantity", "gmv", "discount_amount", "state", "city",];

  // @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  //pagination
  totalPages: number = 0;
  totalItems: number = 0;
  currentPage: number = 1;

  constructor(
    private dataService: DataService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    public datePipe: DatePipe,
  ) { 
    this.getWareHouses();
    this.getStates();
    this.getCities();
  }

  ngOnInit() {
    this.initializeForm();
    this.qryPage = "&page=1";
    this.getReports();
  }

  ngAfterViewInit(): void {
    // this.paginator.page
    //   .pipe(
    //     tap(() => {
    //       this.qryPage = `&page=${this.paginator.pageIndex + 1}`;
    //       this.getReports();
    //     })
    //   )
    //   .subscribe();

  }

  initializeForm() {
    this.filterForm = this.formBuilder.group({
      warehouse: [''],
      state: [''],
      city: [''],
    });
  }
  get f() { return this.filterForm.controls; }

  getWareHouses(){
    // this.loading = true;
    this.dataService.getWareHouses()
      .subscribe(res => {
        // this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        // this.openSnackBar(res.msg, "");
        if (res.status) {
          this.wareHouses = res.data;
        }

      });
  }

  getStates(){
    // this.loading = true;
    this.dataService.getStates()
      .subscribe(res => {
        // this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        // this.openSnackBar(res.msg, "");
        if (res.status) {
          this.states = res.data;
        }
      });
  }

  getCities(){
    // this.loading = true;
    this.dataService.getCities()
      .subscribe(res => {
        // this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        // this.openSnackBar(res.msg, "");
        if (res.status) {
          this.cities = res.data;
        }
      });
  }

  getReports() {
    // console.log(TAG+"GET GRN :+> PAGE:"+);

    // var qryPage = `&page=${page}`;  
    var qryParam = this.qryPage.concat(this.qryFilter);
    this.loading = true;
    this.dataService.getReportsData({ qryParam })
      .subscribe(res => {
        this.loading = false;
        console.log(TAG + "Res : " + JSON.stringify(res));
        this.openSnackBar(res.msg, "");
        if (res.status) {
          this.reports = res.data;
          // this.totalPages = res.total_pages;
          // this.totalItems = res.total;
          // this.currentPage = res.page;
        }

      });
  }


  onApplyFilter() {
    // console.log(TAG+"VArification :"+this.f.verified_status.value);

    let filter = {
      warehouse: this.f.warehouse.value != "undefined" ? this.f.warehouse.value : "",
      state: this.f.state.value != "undefined" ? this.f.state.value : "",
      city: this.f.city.value != "undefined" ? this.f.city.value : "",

    };
    this.qryFilter = "";

    this.qryFilter = this.qryFilter.concat(filter.warehouse == "" ? "" : `&warehouse_id=${filter.warehouse}`);
    this.qryFilter = this.qryFilter.concat(filter.state == "" ? "" : `&state=${filter.state}`);
    this.qryFilter = this.qryFilter.concat(filter.city == "" ? "" : `&city=${filter.city}`);

    this.getReports();
    console.log(TAG + "Applied Filter :" + JSON.stringify(filter));
    console.log(TAG + "Applied Filter :" + JSON.stringify(this.qryFilter));
  }

  //Util
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  formateDate(date: Date) {
    if (!isNaN(date.getMilliseconds()))
      return this.datePipe.transform(date, 'yyyy-MM-dd');
    else
      return "";
  }

}
