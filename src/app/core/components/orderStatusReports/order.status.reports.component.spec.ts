import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderStatusReportsComponent } from './order.status.reports.component';

describe('OrderStatusReportsComponent', () => {
  let component: OrderStatusReportsComponent;
  let fixture: ComponentFixture<OrderStatusReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderStatusReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderStatusReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
