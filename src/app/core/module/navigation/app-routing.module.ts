import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from "../../utils";
//Components
import { HomeComponent } from "../../components/home/home.component";
import { LoginComponent } from "../../components/login/login.component";
import { GrnComponent } from "../../components/grn/grn.component";
import { WarehouseTransactionsComponent } from "../../components/warehouse-transactions/warehouse-transactions.component";
import { WareHouseTransactionSummeryComponent } from "../../components/ware-house-transaction-summery/ware-house-transaction-summery.component";
import { OrderStatusReportsComponent } from "../../components/orderStatusReports/order.status.reports.component";
import { WareHouseReportsComponent } from '../../components/ware-house-reports/ware-house-reports.component';
import { StateReportsComponent } from '../../components/state-reports/state-reports.component';
import { CityReportsComponent } from '../../components/city-reports/city-reports.component';
//

const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [AuthGuard] },

  { path: "login", component: LoginComponent },

  {
    path: "home", component: HomeComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'grn', pathMatch: 'full' },
      { path: "grn", component: GrnComponent, canActivate: [AuthGuard] },
      { path: "warehouse_transaction", component: WarehouseTransactionsComponent, canActivate: [AuthGuard],
      // children:[
      //   { path: "warehouse_transaction_summery", component: WareHouseTransactionSummeryComponent, canActivate: [AuthGuard],},
      // ]
      },
      { path: "warehouse_transaction_summery", component: WareHouseTransactionSummeryComponent, canActivate: [AuthGuard],},
      { path: "order_status_reports", component: OrderStatusReportsComponent, canActivate: [AuthGuard],},
      { path: "warehouse_reports", component: WareHouseReportsComponent, canActivate: [AuthGuard],},
      { path: "state_reports", component: StateReportsComponent, canActivate: [AuthGuard],},
      { path: "city_reports", component: CityReportsComponent, canActivate: [AuthGuard],}

    ]
  },

]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      onSameUrlNavigation: "reload"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
