import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    ) {

    }

  getGrnData({qryParam}){

    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}

    return this.http.get<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/grn/list${qryParam}`,header)
      .pipe(map(res => {
        return res;
      }));
  }

  verifyUnVerifyGRN({payload}){
    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}

    return this.http.post<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/grn/grn-verified-status`,payload,header)
      .pipe(map(res => {
        return res;
      }));
  }

  getTransactions({qryParam}){
    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}

    return this.http.get<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/transaction${qryParam}`,header)
      .pipe(map(res => {
        return res;
      }));
  }

  getTransactionsSummery({qryParam}){
    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}

    return this.http.get<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/transaction/summary${qryParam}`,header)
      .pipe(map(res => {
        return res;
      }));
  }

  getReportsData({qryParam}){

    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}
    return this.http.get<any>(`https://stagingapi.dealshare.in/index.php?r=v1/report/order-status-report${qryParam}`,header)
      .pipe(map(res => {
        return res;
      }));
  }

  getWareHouses(){
    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}
    return this.http.get<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/common/warehouse`,header)
      .pipe(map(res => {
        return res;
      }));
  }

  getStates(){
      let header={headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
      })}
      return this.http.get<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/common/state`,header)
        .pipe(map(res => {
          return res;
        }));
    }

  getCities(){
    let header={headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authenticationService.getUserToken,
    })}
    return this.http.get<any>(`https://stagpoapi.dealshare.in/index.php?r=finance/common/city`,header)
      .pipe(map(res => {
        return res;
      }));
  }  
}
