import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common'
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {AppRoutingModule} from "./core/module/navigation/app-routing.module";
import { MaterialModule } from "./shared/modules/index";
// import { CdkTableExporterModule } from 'cdk-table-exporter';
import { MatTableExporterModule } from 'mat-table-exporter';

import { HomeComponent } from './core/components/home/home.component';
import { LoginComponent } from './core/components/login/login.component';
import { from } from 'rxjs';
import { GrnComponent } from './core/components/grn/grn.component';
import { GrnVerificationDialogComponent } from './core/components/grn-verification-dialog/grn-verification-dialog.component';
import { WarehouseTransactionsComponent } from './core/components/warehouse-transactions/warehouse-transactions.component';
import { WareHouseTransactionSummeryComponent } from './core/components/ware-house-transaction-summery/ware-house-transaction-summery.component';
import { GrnDetailDialogComponent } from './core/components/grn-detail-dialog/grn-detail-dialog.component';
import { OrderStatusReportsComponent } from './core/components/orderStatusReports/order.status.reports.component';
import { WareHouseReportsComponent } from './core/components/ware-house-reports/ware-house-reports.component';
import { StateReportsComponent } from './core/components/state-reports/state-reports.component';
import { CityReportsComponent } from './core/components/city-reports/city-reports.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    GrnComponent,
    GrnVerificationDialogComponent,
    WarehouseTransactionsComponent,
    WareHouseTransactionSummeryComponent,
    GrnDetailDialogComponent,
    OrderStatusReportsComponent,
    WareHouseReportsComponent,
    StateReportsComponent,
    CityReportsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    
    //Network
    HttpClientModule,
    //*Network
    
    // Form
    FormsModule,
    ReactiveFormsModule,
    //* Form
    
    // Material
    MaterialModule,
    //* Material
    // CdkTableExporterModule,
    MatTableExporterModule,
    
    // App
    AppRoutingModule, 
    //* App

  ],
  entryComponents: [
    GrnVerificationDialogComponent,
    GrnDetailDialogComponent
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
